﻿using RafalOwczarski.AccountAndProfile;
using RafalOwczarski.SaveLoad;
using UnityEngine;
using Zenject;

namespace RafalOwczarski.AutoSaver
{
    class AutoSave : IAutoSave, ITickable
    {
        [Inject] private ISaveLoadService saveLoadService;
        [Inject] private IAccountProvider accountProvider;

        private float timeSinceLastCheck = 0;
        private float checkDelay = 30;
        private bool saveRequested = false;
        public void ForceSave()
        {
            SaveAccount();
        }

        public void RequestSave()
        {
            saveRequested = true;
        }

        private void SaveAccount()
        {
            Account account = accountProvider.GetAccount();
            saveLoadService.SaveAccount(account);
        }
        public void Tick()
        {
            timeSinceLastCheck += Time.deltaTime;

            if (timeSinceLastCheck > checkDelay)
            {
                timeSinceLastCheck = 0;

                if(saveRequested)
                {
                    saveRequested = false;
                    SaveAccount();
                }
            }
        }
    }
}
