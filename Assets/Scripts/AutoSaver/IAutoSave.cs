﻿namespace RafalOwczarski.AutoSaver
{
    public interface IAutoSave
    {
        public void RequestSave();
        public void ForceSave();
    }
}
