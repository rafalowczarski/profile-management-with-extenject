using System;
namespace RafalOwczarski.AccountAndProfile
{
    [Serializable]
    public class Profile
    {
        public OverallProfileData OveralProfileData { get; private set; }
        public CharacterData CharacterData { get; private set; }

        public Profile()
        {
            OveralProfileData = new OverallProfileData();
            CharacterData = new CharacterData();
        }
    }
}
