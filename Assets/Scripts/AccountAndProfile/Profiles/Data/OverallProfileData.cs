﻿
using System;

namespace RafalOwczarski.AccountAndProfile
{
    [Serializable]
    public class OverallProfileData: BaseProfileData
    {
        public int experience;
        public int softCurrency;
    }
}
