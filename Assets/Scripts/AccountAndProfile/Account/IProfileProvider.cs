﻿namespace RafalOwczarski.AccountAndProfile
{
    public interface IProfileProvider
    {
        public Profile GetProfile();
    }
}
