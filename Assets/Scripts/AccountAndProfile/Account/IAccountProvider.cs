﻿namespace RafalOwczarski.AccountAndProfile
{
    public interface IAccountProvider
    {
        public Account GetAccount();
    }
}
