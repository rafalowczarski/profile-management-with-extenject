﻿using System;

namespace RafalOwczarski.AccountAndProfile
{
    [Serializable]
    public class Account
    {
        public Profile Profile { get; private set; }

        public Account()
        {
            Profile = new Profile();
        }
    }
}
