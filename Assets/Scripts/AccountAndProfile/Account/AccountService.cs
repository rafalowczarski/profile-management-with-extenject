﻿using RafalOwczarski.SaveLoad;
using Zenject;

namespace RafalOwczarski.AccountAndProfile
{
    class AccountService : IInitializable, IAccountProvider, IProfileProvider
    {
        [Inject] ISaveLoadService SaveLoadService;

        private Account account;

        public Account GetAccount() => account;

        public Profile GetProfile() => account.Profile;

        public void Initialize()
        {
            account = SaveLoadService.LoadAccount();
        }
    }
}
