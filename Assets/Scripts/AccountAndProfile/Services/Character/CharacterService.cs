﻿using Zenject;

namespace RafalOwczarski.AccountAndProfile
{
    class CharacterService : BaseProfileService<CharacterData>, ICharacterService
    {
        protected override CharacterData Data => currentProfile.CharacterData;
    }
}
