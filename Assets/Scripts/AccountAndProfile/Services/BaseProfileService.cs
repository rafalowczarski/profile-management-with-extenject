﻿using Zenject;

namespace RafalOwczarski.AccountAndProfile
{
    public abstract class BaseProfileService<T> where T : BaseProfileData
    {
        [Inject] private IProfileProvider profileProvider;

        protected Profile currentProfile => profileProvider.GetProfile();
        protected abstract T Data { get; }
    }
}
