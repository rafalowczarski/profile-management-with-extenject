using Zenject;

namespace RafalOwczarski.AccountAndProfile
{
    public class OverallProfileService : BaseProfileService<OverallProfileData>, IOverallProfileService
    {
        protected override OverallProfileData Data => currentProfile.OveralProfileData;
        public int SoftCurency => Data.softCurrency;
        public int Experience => Data.experience;
        public void AddExperience(int amount) => Data.experience += amount;
        public void AddSoftCurrency(int amount) => Data.softCurrency += amount;
        public void RemoveSoftCurrency(int amount) => Data.softCurrency -= amount;
    }
}