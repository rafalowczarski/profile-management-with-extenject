namespace RafalOwczarski.AccountAndProfile
{
    public interface IOverallProfileService
    {
        public void AddExperience(int amount);
        public void AddSoftCurrency(int amount);
        public void RemoveSoftCurrency(int amount);
        public int SoftCurency { get; }
        public int Experience { get; }
    }


}