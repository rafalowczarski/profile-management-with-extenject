using RafalOwczarski.AccountAndProfile;
using RafalOwczarski.AutoSaver;
using System;
using TMPro;
using UnityEngine.UI;
using Zenject;

public class ProfileSaverTest :IInitializable, ITickable, IDisposable
{
    [Inject] Data data;
    [Inject] IOverallProfileService overallProfileService;
    [Inject] IAutoSave autoSave;
    public void Initialize()
    {
        data.SaveButton.onClick.AddListener(OnSaveButtonClick);
        data.AddExperience.onClick.AddListener(OnAddExperienceButtonClick);
    }

    private void OnAddExperienceButtonClick()
    {
        overallProfileService.AddExperience(1);
    }

    private void OnSaveButtonClick()
    {
        autoSave.ForceSave();
    }

    public void Tick()
    {
        data.expText.text = $"{overallProfileService.Experience}";
    }

    public void Dispose()
    {
        data.SaveButton.onClick.RemoveListener(OnSaveButtonClick);
        data.AddExperience.onClick.RemoveListener(OnAddExperienceButtonClick);
    }

    [Serializable]
    public class Data
    {
        public Button SaveButton;
        public Button AddExperience;
        public TMP_Text expText;
    }
    
}
