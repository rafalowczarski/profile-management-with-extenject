using RafalOwczarski.AccountAndProfile;
using RafalOwczarski.AutoSaver;
using RafalOwczarski.SaveLoad;
using UnityEngine;
using Zenject;

public class InitialInstaller : MonoInstaller<InitialInstaller>
{
    [SerializeField] ProfileSaverTest.Data profileSaver;
    public override void InstallBindings()
    {

        Container.BindInterfacesAndSelfTo<SaveLoadServiceMock>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<AccountService>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<AutoSave>().AsSingle().NonLazy();

        Container.BindInterfacesAndSelfTo<OverallProfileService>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<CharacterService>().AsSingle().NonLazy();

        Container.BindInstance(profileSaver);
        Container.BindInterfacesAndSelfTo<ProfileSaverTest>().AsSingle().NonLazy();

    }
}

