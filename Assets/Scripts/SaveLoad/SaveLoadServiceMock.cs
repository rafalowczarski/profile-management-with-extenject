using Newtonsoft.Json;
using RafalOwczarski.AccountAndProfile;
using System.IO;
using UnityEngine;

namespace RafalOwczarski.SaveLoad
{
    public class SaveLoadServiceMock : ISaveLoadService
    {
        private const string DIRECTORY = "/";
        private const string ACCOUNT_SAVE_FILE_NAME = "WeatherForecast.json";
        private string accountFilePath = $"{Application.persistentDataPath}{DIRECTORY}{ACCOUNT_SAVE_FILE_NAME}";
        public Account LoadAccount()
        {
            Account account = null;

            if (File.Exists(accountFilePath))
            {
                string json = File.ReadAllText(accountFilePath);
                account = JsonConvert.DeserializeObject<Account>(json);
            }
            else
            {
                account = new Account();
                SaveAccount(account);
            }

            return account;
        }

        public void SaveAccount(Account account)
        {
            string json = JsonConvert.SerializeObject(account);
            File.WriteAllText(accountFilePath, json);
        }
    }
}