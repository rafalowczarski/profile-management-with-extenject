using RafalOwczarski.AccountAndProfile;

namespace RafalOwczarski.SaveLoad
{
    public interface ISaveLoadService
    {
        public Account LoadAccount();
        public void SaveAccount(Account profile);
    }
}

